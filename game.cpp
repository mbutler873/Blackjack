#include "game.h"

#include "iomanip"

void
Game::play(Deck& d1, Player& dealer, Player& p1)
{
  setup(d1, dealer, p1);

  bool bust = false;
  while(p1.score < 17)
  {
    deal(d1,p1,1);
    std::cout << "\nPlayer Hand: ";
    print(p1);
    updateScore(p1);
    if(p1.checkBust())
    {
      std::cout << "Player Score: " << p1.score << '\n';
      std::cout << "Bust! Player Loses\n";
      bust = true;
      ++dealerWins;
      continue;
    }
    std::cout << "Player score: " << p1.score << '\n';
  }

  while((dealer.score < 17 || (dealer.score == 17 && dealer.ace_eleven > 0)) && bust == false)
  {
    deal(d1,dealer,1);
    std::cout << "\nDealer Hand: ";
    print(dealer);
    updateScore(dealer);
    if(dealer.checkBust())
    {
      std::cout << "Dealer Score: " << dealer.score << '\n';
      std::cout << "Bust! Player Wins\n";
      bust = true;
      ++playerWins;
      continue;
    }
    std::cout << "Dealer Score: " << dealer.score << '\n';
  }

  if(bust == false)
  {
    if(p1.score > dealer.score)
    {
      ++playerWins;
      std::cout << "Player Wins\n";
    }
    else if(p1.score < dealer.score)
    {
      ++dealerWins;
      std::cout << "Player Loses\n";
    }
    else
    {
      ++push;
      std::cout << "Push\n";
    }
  }

  p1.hand.clear();
  dealer.hand.clear();
  p1.score = 0;
  dealer.score = 0;
  p1.ace_eleven = 0;
  dealer.ace_eleven = 0;
  std::cout << "****************\n";
}

void
Game::setup(Deck& d1, Player& dealer, Player& p1)
{
  d1 = make_standard_deck();
  shuffle(d1);
  deal(d1,dealer,1);
  std::cout << "\nDealer Hand: ";
  print(dealer);
  updateScore(dealer);
  std::cout << "Dealer Score: " << dealer.score << '\n';
  deal(d1, p1, 1);
  updateScore(p1);
  deal(d1, p1, 1);
  updateScore(p1);
  std::cout << "\nPlayer Hand: ";
  print(p1);
  p1.checkBust();
  std::cout << "Player score: " << p1.score << '\n';
}

bool
Game::getHit()
{
  char choice;
  while(!(choice == 'S' || choice == 'H'))
  {
    std::cout << "S for stand, H for hit: ";
    std::cin >> choice;
  }
  if(choice == 'H')
    return true;
  else if(choice == 'S')
    return false;
}