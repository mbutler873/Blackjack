#ifndef GAME_H
#define GAME_H

#include "deck.h"
#include "card.h"
#include "Player.h"

struct Game
{
  void play(Deck& d1, Player& dealer, Player& p1);
  void setup(Deck& d1, Player& dealer, Player& p1);
  bool getHit();

  int dealerWins = 0;
  int playerWins = 0;
  int push = 0;
};

#endif // GAME_H