#include "player.h"

#include <iostream>
#include <random>
#include <algorithm>
#include <iterator>

void
print(Player p)
{
  int i = 1;
  for (int j = 0; j < p.hand.size(); ++j) {
    std::cout << p.hand[j] << ' ';
      i = 0;
    ++i;
  }
  std::cout << '\n';
}

void
updateScore(Player& p)
{
  if(p.hand.back().get_rank() < 10)
  {
    p.score += p.hand.back().get_rank();
  }
  else if(p.hand.back().get_rank() == 14)
  {
    p.score += 11;
    ++p.ace_eleven;
  }
  else
  {
    p.score += 10;
  }
}

bool
Player::checkBust()
{
  if(score > 21)
  {
    if(ace_eleven > 0)
    {
      score -= 10;
      --ace_eleven;
      return false;
    }
    else
    {
      return true;
    }
  }
  else
  {
    return false;
  }
}