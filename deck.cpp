#include "deck.h"

#include <iostream>
#include <random>
#include <algorithm>

Deck
make_standard_deck()
{
  Deck d {
    {Ace, Spades},
    {Two, Spades},
    {Three, Spades},
    {Four, Spades},
    {Five, Spades},
    {Six, Spades},
    {Seven, Spades},
    {Eight, Spades},
    {Nine, Spades},
    {Ten, Spades},
    {Jack, Spades},
    {Queen, Spades},
    {King, Spades},

    {Ace, Clubs},
    {Two, Clubs},
    {Three, Clubs},
    {Four, Clubs},
    {Five, Clubs},
    {Six, Clubs},
    {Seven, Clubs},
    {Eight, Clubs},
    {Nine, Clubs},
    {Ten, Clubs},
    {Jack, Clubs},
    {Queen, Clubs},
    {King, Clubs},

    {Ace, Hearts},
    {Two, Hearts},
    {Three, Hearts},
    {Four, Hearts},
    {Five, Hearts},
    {Six, Hearts},
    {Seven, Hearts},
    {Eight, Hearts},
    {Nine, Hearts},
    {Ten, Hearts},
    {Jack, Hearts},
    {Queen, Hearts},
    {King, Hearts},

    {Ace, Diamonds},
    {Two, Diamonds},
    {Three, Diamonds},
    {Four, Diamonds},
    {Five, Diamonds},
    {Six, Diamonds},
    {Seven, Diamonds},
    {Eight, Diamonds},
    {Nine, Diamonds},
    {Ten, Diamonds},
    {Jack, Diamonds},
    {Queen, Diamonds},
    {King, Diamonds},
  };
  return d;
}

Deck
make_combined_deck(const Deck& d1, const Deck& d2)
{
  Deck d;
  d.insert(d.end(), d1.begin(), d1.end());
  d.insert(d.end(), d2.begin(), d2.end());
  return d;
}

void
shuffle(Deck& d)
{
  // Function-local extern variable.
  extern std::minstd_rand prng;

  std::shuffle(d.begin(), d.end(), prng);
}


void
deal(Deck& d, Player& p1, Player& p2)
{
  while(!(d.empty()))
  {
    p1.hand.push_back(*(d.end() - 1));
    d.pop_back();
    p2.hand.push_back(*(d.end() - 1));
    d.pop_back();
  }
}

void deal(Deck& d, Player& p1, int num)
{
  for(int i = 0; i < num; ++i)
  {
    if(d.empty())
    {
        std::cout << "Not enough cards\n";
        return;
    }
    p1.hand.push_back(d.back());
    d.pop_back();
  }
}

void
print(const Deck& deck)
{
  for (Card c : deck) {
    std::cout << c << ' ';
  }
  std::cout << '\n';
}
