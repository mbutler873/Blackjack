#pragma once
#include "card.h"

#include <vector>
#include <iostream>

struct Player
{
  bool checkBust();

  std::vector<Card> hand;
  int score = 0;
  int ace_eleven = 0;
};

void print(Player);

void updateScore(Player& p);