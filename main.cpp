#include "Player.h"
#include "card.h"
#include "deck.h"
#include "game.h"

#include <iostream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <random>
#include <time.h>


std::random_device rng;
std::minstd_rand prng;

int main()
{
  prng.seed(time(0));

  Player dealer;
  Player p1;
  Deck d1 = make_standard_deck();

  Game g1;
  double trials = 10;
  for(int i = 0; i < trials; ++i)
  {
    g1.play(d1, dealer, p1);
  }

  std::cout << "Player Won : " << g1.playerWins/trials*100 << "%\n";
  std::cout << "Dealer Won : " << g1.dealerWins/trials*100 << "%\n";
  std::cout << "Push Won : " << g1.push/trials*100 << "%\n";
}