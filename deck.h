#include "card.h"
#include "player.h"

#include <vector>

// Pragmatic approach. Don't write a class if you don't
// need to.
using Deck = std::vector<Card>;

// Make a standard deck of cards.
Deck make_standard_deck();

// Combine two decks of cards.
Deck make_combined_deck(const Deck& d1, const Deck& d2);

// Shuffle a deck of cards.
void shuffle(Deck& d);

// Deal cards to 2 players
void deal(Deck& d, Player& p1, Player& p2);

// Deal certain number of cards to 1 player
void deal(Deck& d, Player& p1, int num);

// Print a deck of cards.
void print(const Deck& d);